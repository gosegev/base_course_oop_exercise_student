package Enums;

public enum FlightStatues {
	READY_TO_FLIGHT,
	IN_AIR,
	NOT_READY_TO_FLIGHT;
}