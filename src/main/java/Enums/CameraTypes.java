package Enums;

public enum CameraTypes {
	REGULAR,
	THERMAL,
	NIGHT_VISION;
}