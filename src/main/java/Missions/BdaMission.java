package Missions;

import Abilities.EnsureAttack;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.Abilities;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class BdaMission extends Mission {

	private String objective;

	public BdaMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String objective) throws AerialVehicleNotCompatibleException, IOException, ParseException {
		super(destination, pilotName, aerialVehicle);
		this.objective = objective;
	}

	@Override
	public String getAbilityName() {
		return "EnsureAttack";
	}

	@Override
	public String executeMission() {
		return super.executeMission() + " talking pictures of  " + this.objective + " with: " + ((EnsureAttack) (this.extractForMission(Abilities.EnsureAttack.name()))).getCameraTypes();
	}

}