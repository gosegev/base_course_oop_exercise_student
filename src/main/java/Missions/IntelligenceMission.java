package Missions;

import Abilities.IntelligenceAbility;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.Abilities;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class IntelligenceMission extends Mission {
	private String region;

	public IntelligenceMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String region) throws AerialVehicleNotCompatibleException, IOException, ParseException {
		super(destination, pilotName, aerialVehicle);
		this.region = region;
	}

	@Override
	public String getAbilityName() {
		return "IntelligenceAbility";
	}

	@Override
	public String executeMission() {
		return super.executeMission() + " Collecting Data in " + this.region + " with: " + ((IntelligenceAbility) (this.extractForMission(Abilities.IntelligenceAbility.name()))).getSensorType();
	}

	public String getRegion() {
		return this.region;
	}
}