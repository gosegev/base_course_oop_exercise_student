package Missions;

import Abilities.Ability;
import Abilities.AbilityValidation;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public abstract class Mission {


	private Coordinates destination;
	private String pilotName;
	private AerialVehicle aerialVehicle;


	public Mission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle) throws AerialVehicleNotCompatibleException, IOException, ParseException {
		this.destination = destination;
		this.pilotName = pilotName;
		this.setAerialVehicle(aerialVehicle);
	}

	public String getMissionName() {
		return this.getClass().getSimpleName();
	}

	public String getAbilityName() {
		return "";
	}

	public Coordinates getDestination() {
		return this.destination;
	}

	public String getPilotName() {
		return this.pilotName;
	}

	public AerialVehicle getAerialVehicle() {
		return this.aerialVehicle;
	}

	public void setAerialVehicle(AerialVehicle aerialVehicle) throws AerialVehicleNotCompatibleException, IOException, ParseException {
		if (AbilityValidation.isAbilityExistsInAerialVehiclesAbilityList(aerialVehicle.getAbilities(), this.getMissionName())) {
			this.aerialVehicle = aerialVehicle;
		} else {
			throw new AerialVehicleNotCompatibleException(" Ability is missing in  Aerial vehicle for executing " + this.getMissionName());
		}

	}

	public String executeMission() {
		return this.pilotName + " " + this.aerialVehicle.getPlaneName();
	}

	public void begin() {
		this.aerialVehicle.flyTo(this.destination);
		System.out.println("beginning Mission!");
	}

	public void cancel() {
		this.aerialVehicle.land(this.destination);
		System.out.println("Abort Mission");
	}

	public void finish() {
		System.out.println(this.executeMission());
		this.aerialVehicle.land(this.destination);
		System.out.println("Finish Mission");
	}

	public Ability extractForMission(String abilityName) {
		Ability matchAbility = this.getAerialVehicle().getAbilities().get(0);
		for (int i = 0; i < this.getAerialVehicle().getAbilities().size(); i++) {
			if (this.getAerialVehicle().getAbilities().get(i).getAbilityName().equals(abilityName))
				matchAbility = this.getAerialVehicle().getAbilities().get(i);
		}
		return matchAbility;
	}
}
