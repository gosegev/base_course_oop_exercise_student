package Missions;

import Abilities.AttackAbility;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.Abilities;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class AttackMission extends Mission {
	private String target;

	public AttackMission(String target, Coordinates destination, String pilotName, AerialVehicle aerialVehicle) throws AerialVehicleNotCompatibleException, IOException, ParseException {
		super(destination, pilotName, aerialVehicle);
		this.target = target;
	}

	@Override
	public String getAbilityName() {
		return "AttackAbility";
	}

	@Override
	public String executeMission() {
		return super.executeMission() + " Attacking " + this.target + " with: " + ((AttackAbility) (this.extractForMission(Abilities.AttackAbility.name()))).getRocketTypes() + "X" + ((AttackAbility) (this.extractForMission(Abilities.AttackAbility.name()))).getRocketsAmount();
	}
}