package Abilities;

import Enums.RocketTypes;

public class AttackAbility extends Ability {

	private int rocketsAmount;
	private RocketTypes rocketTypes;

	public AttackAbility(int rocketsAmount, RocketTypes rocketTypes) {
		this.rocketsAmount = rocketsAmount;
		this.rocketTypes = rocketTypes;
	}

	public int getRocketsAmount() {
		return this.rocketsAmount;
	}

	public RocketTypes getRocketTypes() {
		return this.rocketTypes;
	}

}