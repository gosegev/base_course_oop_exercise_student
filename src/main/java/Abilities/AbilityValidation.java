package Abilities;

import AerialVehicles.AerialVehicle;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class AbilityValidation {

	private static JSONObject AerialVehicleAbilities;
	private static JSONObject MissionsAbilities;

	public static JSONObject getAerialVehicleAbilities() throws IOException, ParseException {
		if (AerialVehicleAbilities == null) {
			AerialVehicleAbilities = (JSONObject) new JSONParser().parse(new FileReader("src/main/java/resources/AerialVehiclesAbilities.json"));
		}
		return AerialVehicleAbilities;
	}

	public static JSONObject getMissionsAbilities() throws IOException, ParseException {
		if (MissionsAbilities == null) {
			MissionsAbilities = (JSONObject) new JSONParser().parse(new FileReader("src/main/java/resources/MissionsAbilities.json"));
		}
		return MissionsAbilities;
	}

	public static boolean isAbilityInAerialVehicles(AerialVehicle aerialVehicle, Ability ability) {
		try {
			return getAerialVehicleAbilities().get(aerialVehicle.getPlaneName()).toString().contains(ability.getAbilityName());
		} catch (IOException | ParseException e) {
			System.out.println("error: " + e.getMessage());
		}
		return false;
	}

	public static boolean isAbilityExistsInAerialVehiclesAbilityList(List<Ability> AerialVehiclesAbilityList, String missionName) throws IOException, ParseException {
		JSONArray missionRequiredAbilities = (JSONArray) getMissionsAbilities().get(missionName);
		return missionRequiredAbilities.stream().allMatch(abilityName -> AerialVehiclesAbilityList.stream().anyMatch(ability -> ability.getAbilityName().equals(abilityName)));

	}
}