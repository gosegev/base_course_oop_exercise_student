package Abilities;

import Enums.CameraTypes;

public class EnsureAttack extends Ability{
	CameraTypes cameraTypes;

	public EnsureAttack(CameraTypes cameraTypes) {
		this.cameraTypes = cameraTypes;
	}

	public CameraTypes getCameraTypes() {
		return this.cameraTypes;
	}
}