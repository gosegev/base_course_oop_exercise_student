package Abilities;

import Enums.SensorTypes;

public class IntelligenceAbility extends Ability {
	SensorTypes sensorTypes;

	public IntelligenceAbility(SensorTypes cameraTypes) {
		this.sensorTypes = cameraTypes;
	}

	public SensorTypes getSensorType() {
		return this.sensorTypes;
	}
}