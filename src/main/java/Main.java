import Abilities.Ability;
import Abilities.AttackAbility;
import Abilities.EnsureAttack;
import Abilities.IntelligenceAbility;
import AerialVehicles.F15;
import AerialVehicles.Kochav;
import AerialVehicles.Shoval;
import Entities.Coordinates;
import Enums.CameraTypes;
import Enums.FlightStatues;
import Enums.RocketTypes;
import Enums.SensorTypes;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) throws AerialVehicleNotCompatibleException, IOException, ParseException {
		List<Ability> abilityF15 = new ArrayList<>();
		abilityF15.add(new AttackAbility(30, RocketTypes.AMRAM));
		F15 f15 = new F15(400, FlightStatues.READY_TO_FLIGHT, new Coordinates(15.3, 43.5), abilityF15);

		f15.flyTo(new Coordinates(50.3, 30.5));
		f15.land(new Coordinates(50.3, 30.5));
		System.out.println(f15.getFlightStatues());

		System.out.println("------------------------------------------------------------------------------------");

		List<Ability> abilityShoval = new ArrayList<>();
		abilityShoval.add(new AttackAbility(30, RocketTypes.AMRAM));
		abilityShoval.add(new IntelligenceAbility(SensorTypes.ELINT));
		Shoval shoval = new Shoval(400, FlightStatues.READY_TO_FLIGHT, new Coordinates(15.3, 43.5), abilityShoval);

		shoval.flyTo(new Coordinates(50.3, 30.5));
		shoval.land(new Coordinates(50.3, 30.5));
		System.out.println(shoval.hoverOverLocation(new Coordinates(50.3, 30.5)));

		System.out.println("------------------------------------------------------------------------------------");


		List<Ability> abilityKochav = new ArrayList<>();
		abilityKochav.add(new AttackAbility(30, RocketTypes.AMRAM));
		abilityKochav.add(new EnsureAttack(CameraTypes.REGULAR));
		Kochav kochav = new Kochav(400, FlightStatues.READY_TO_FLIGHT, new Coordinates(15.3, 43.5), abilityKochav);

		kochav.flyTo(new Coordinates(50.3, 30.5));
		kochav.land(new Coordinates(50.3, 30.5));
		System.out.println(kochav.hoverOverLocation(new Coordinates(50.3, 30.5)));

		System.out.println("---------------------------------------------------------------------------------------");

		AttackMission attackMission = new AttackMission("Gaza", new Coordinates(12.5, 32.5), "Ofri", f15);
		attackMission.begin();
		attackMission.cancel();
		attackMission.finish();

		System.out.println("---------------------------------------------------------------------------------------");

		IntelligenceMission intelligenceMission = new IntelligenceMission(new Coordinates(12.5, 32.5),"Ofri",shoval,"sfad" );
		intelligenceMission.begin();
		intelligenceMission.cancel();
		intelligenceMission.finish();

		System.out.println("---------------------------------------------------------------------------------------");

		BdaMission bdaMission = new BdaMission(new Coordinates(12.5, 32.5),"Ofri",kochav,"sfad" );
		bdaMission.begin();
		bdaMission.cancel();
		bdaMission.finish();




//		List<Ability> abilityShoval = new ArrayList<>();
//		abilityShoval.add(new EnsureAttack(CameraTypes.NIGHT_VISION));





	}
}