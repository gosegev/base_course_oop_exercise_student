package AerialVehicles;

import Abilities.Ability;
import Entities.Coordinates;
import Enums.FlightStatues;
import Missions.AerialVehicleNotCompatibleException;

import java.util.List;

public abstract class FighterAircraft extends AerialVehicle {

	public FighterAircraft( int flightHours, FlightStatues flightStatues, Coordinates homeBase, List<Ability> abilities) throws AerialVehicleNotCompatibleException {
		super(250, flightHours, flightStatues, homeBase, abilities);
	}
}