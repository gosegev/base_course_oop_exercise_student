package AerialVehicles;


import Abilities.Ability;
import Abilities.AbilityValidation;
import Entities.Coordinates;
import Enums.FlightStatues;
import Missions.AerialVehicleNotCompatibleException;

import java.util.List;

public abstract class AerialVehicle implements IAerialVehicle {

	private int timeTreatment;
	private int flightHours;
	private FlightStatues flightStatues;
	private Coordinates homeBase;
	private List<Ability> abilities;

	public AerialVehicle(int timeTreatment, int flightHours, FlightStatues flightStatues, Coordinates homeBase, List<Ability> abilities) throws AerialVehicleNotCompatibleException {
		this.timeTreatment = timeTreatment;
		this.flightHours = flightHours;
		this.flightStatues = flightStatues;
		this.homeBase = homeBase;
		this.setAbilities(abilities);
	}

	@Override
	public void flyTo(Coordinates coordinates) {
		System.out.println("flying to " + coordinates.toString());

		if (this.flightStatues == FlightStatues.NOT_READY_TO_FLIGHT) {
			System.out.println("Aerial Vehicle is not ready to fly");
		} else {
			this.flightStatues = FlightStatues.IN_AIR;
		}
	}

	@Override
	public void land(Coordinates coordinates) {
		System.out.println("landing on " + coordinates.toString());
		this.check();
	}

	@Override
	public void check() {
		if (this.flightHours > this.timeTreatment) {
			this.flightStatues = FlightStatues.NOT_READY_TO_FLIGHT;
			this.repair();
		} else {
			this.flightStatues = FlightStatues.READY_TO_FLIGHT;
		}
	}

	@Override
	public void repair() {
		this.flightHours = 0;
		this.flightStatues = FlightStatues.READY_TO_FLIGHT;
	}

	public String getPlaneName() {
		return this.getClass().getSimpleName();
	}

	public int getFlightHours() {
		return this.flightHours;
	}

	public void setFlightHours(int flightHours) {
		this.flightHours = flightHours;
	}

	public FlightStatues getFlightStatues() {
		return this.flightStatues;
	}

	public void setFlightStatues(FlightStatues flightStatues) {

		this.flightStatues = flightStatues;
	}


	public Coordinates getHomeBase() {
		return this.homeBase;
	}

	public List<Ability> getAbilities() {
		return this.abilities;
	}

	public int getTimeTreatment() {
		return timeTreatment;
	}

	public void setAbilities(List<Ability> abilities) throws AerialVehicleNotCompatibleException {
		if (abilities.stream().allMatch((ability -> AbilityValidation.isAbilityInAerialVehicles(this,ability)))) {
			this.abilities = abilities;
		}
		else{
			throw new AerialVehicleNotCompatibleException("not all the abilities exists in Aerial vehicle");
		}
	}
}
