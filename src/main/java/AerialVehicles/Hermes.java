package AerialVehicles;

import Abilities.Ability;
import Entities.Coordinates;
import Enums.FlightStatues;
import Missions.AerialVehicleNotCompatibleException;

import java.util.List;

public abstract class Hermes extends Uav {

	public Hermes(int flightHours, FlightStatues flightStatues, Coordinates homeBase, List<Ability> abilities) throws AerialVehicleNotCompatibleException {
		super(100, flightHours, flightStatues, homeBase, abilities);
	}
}