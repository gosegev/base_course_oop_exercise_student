package AerialVehicles;

import Abilities.Ability;
import Entities.Coordinates;
import Enums.FlightStatues;
import Missions.AerialVehicleNotCompatibleException;

import java.util.List;

public abstract class Uav extends AerialVehicle {
	public Uav(int timeTreatment, int flightHours, FlightStatues flightStatues, Coordinates homeBase, List<Ability> abilities) throws AerialVehicleNotCompatibleException {
		super(timeTreatment, flightHours, flightStatues, homeBase, abilities);
	}

	public String hoverOverLocation(Coordinates destination) {
		return "Hovering Over: " + destination.toString();
	}
}