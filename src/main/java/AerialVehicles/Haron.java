package AerialVehicles;

import Abilities.Ability;
import Entities.Coordinates;
import Enums.FlightStatues;
import Missions.AerialVehicleNotCompatibleException;

import java.util.List;

public abstract class Haron extends Uav {

	public Haron(int flightHours, FlightStatues flightStatues, Coordinates homeBase, List<Ability> abilities) throws AerialVehicleNotCompatibleException {
		super(150, flightHours, flightStatues, homeBase, abilities);
	}
}