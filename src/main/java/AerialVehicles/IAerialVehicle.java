package AerialVehicles;

import Entities.Coordinates;

public interface IAerialVehicle {
	void flyTo(Coordinates coordinates);
	void land(Coordinates coordinates);
	void check();
	void repair();
}