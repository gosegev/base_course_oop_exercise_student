package AerialVehicles;


import Abilities.Ability;
import Entities.Coordinates;
import Enums.FlightStatues;
import Missions.AerialVehicleNotCompatibleException;

import java.util.List;

public class Zik extends Hermes {
	public Zik(int flightHours, FlightStatues flightStatues, Coordinates homeBase, List<Ability> abilities) throws AerialVehicleNotCompatibleException {
		super(flightHours, flightStatues, homeBase, abilities);
	}
}